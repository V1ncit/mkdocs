# CTF Writeups og andet

## TryHackMe

<figure markdown="span">
    [ ![vnct](https://tryhackme-badges.s3.amazonaws.com/vnct.png)](https://tryhackme.com/p/vnct)
</figure>

### Gennemførte rum i dette projekt:

#### SOC Level 1 - Cyber Defence Frameworks
[Junior Security Analyst Intro](https://tryhackme.com/r/room/jrsecanalystintrouxo)
[Pyramid Of Pain](https://tryhackme.com/r/room/pyramidofpainax)
[Cyber Kill Chain](https://tryhackme.com/r/room/cyberkillchainzmt)
[Unified Kill Chain](https://tryhackme.com/r/room/unifiedkillchain)
[MITRE](https://tryhackme.com/r/room/mitre)

#### SOC Level 1 - Cyber Threat Intelligence
[Intro to Cyber Threat Intel](https://tryhackme.com/r/room/cyberthreatintel)
[Threat Intelligence Tools](https://tryhackme.com/r/room/threatinteltools)
[OpenCTI](https://tryhackme.com/r/room/opencti)




<br>

## HackTheBox

<figure markdown="span">
    [ ![V1nc1t7](https://www.hackthebox.eu/badge/image/452346)](https://www.hackthebox.eu/home/users/profile/452346)
</figure>