Tags: 

# Pyramid of Pain

Room URL: [https://tryhackme.com/r/room/pyramidofpainax](https://tryhackme.com/r/room/pyramidofpainax)

## Intro

This room is a part of the SOC Level 1 Learning path.
I will **NOT** copy-paste everything from the tasks, unless relevant.  

*"Learn what is the Pyramid of Pain and how to utilize this model to determine the level of difficulty it will cause for an adversary to change the indicators associated with them, and their campaign."*

## Task 1 - Introduction

This well-renowned concept is being applied to cybersecurity solutions like [Cisco Security](https://gblogs.cisco.com/ca/2020/08/26/the-canadian-bacon-cisco-security-and-the-pyramid-of-pain/), [SentinelOne](https://www.sentinelone.com/blog/revisiting-the-pyramid-of-pain-leveraging-edr-data-to-improve-cyber-threat-intelligence/), and [SOCRadar](https://socradar.io/re-examining-the-pyramid-of-pain-to-use-cyber-threat-intelligence-more-effectively/) to improve the effectiveness of CTI (Cyber Threat Intelligence), threat hunting, and incident response exercises.

Understanding the Pyramid of Pain concept as a Threat Hunter, Incident Responder, or SOC Analyst is important.

!!! Question

    Read the above.

??? success
    **Answer: Click the button...**

## Task 2 - Hash Values (Trivial)

*"Security professionals usually use the hash values to gain insight into a specific malware sample, a malicious or a suspicious file, and as a way to uniquely identify and reference the malicious artifact."*

!!! Question

    Analyse the report associated with the hash "b8ef959a9176aef07fdca8705254a163b50b49a17217a4ff0107487f59d4a35d" [here](https://assets.tryhackme.com/additional/pyramidofpain/t3-virustotal.pdf). What is the filename of the sample?

If wee take a look at the PDF document, and look for the hash value, we should be able to find the associated file and filename.

![VirusTotal](images/pyramidofpain-1.png)

??? success
    **Answer: Sales_Receipt 5606.xls**

## Task 3 - IP-addresses

!!! Question
    Read the following [report](https://assets.tryhackme.com/additional/pyramidofpain/task3-anyrun.pdf) to answer this question. What is the first IP address the malicious process (PID 1632) attempts to communicate with? 

![VirusTotal](images/pyramidofpain-2.png)

On the very last page in the report, we can see that the first IP the process tries to communicate with.

??? success
    **Answer: 50.87.136.52**