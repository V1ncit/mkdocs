Tags: #analyst #triage #security

# Junior Security Analyst Intro

Room URL: [https://tryhackme.com/r/room/jrsecanalystintrouxo](https://tryhackme.com/r/room/jrsecanalystintrouxo)

## Intro

This room is a part of the SOC Level 1 Learning path.
I will **NOT** copy-paste everything from the tasks, unless relevant.  

*"Play through a day in the life of a Junior Security Analyst, their responsibilities and qualifications needed to land a role as an analyst."*

### Task 1- A career as a Junior (Associate) Security Analyst

Not much of a task, but as this is the introduction to beeing a SOC analyst, this is meerly information and things to think about.

It explains the most common responsibilities and requirements of a Junior Security Analyst or Tier 1 SOC Analyst.

!!! Question "Question 1"
    What will be your role as a Junior Security Analyst?

??? success "Answer"
    Triage Specialist

### Task 2 - Security Operations Center

As this is still a introduction to the learning path, this in exclusively information.

It explains the core function of a SOC is to investigate, monitor, prevent, and respond to cyber threats at any time of the day.

The general subjects of this "task" are **Preparation and Prevention**, **Monitoring and Investigation**, and **Response**.

!!! Question "Question 2"
    Read the above. (In the actual room)

!!! Success "Answer"
    Just click the button!!

### Task 3 - A day In the life of a Junior (Associate) Security Analyst

This "Task" explains as a Junior Security Analyst, they are on the frontlines, monitoring network traffic, analyzing Intrusion Prevention System and alerts from Intrusion Detection Systems, and investigating suspicious emails using forensics and open-source intelligence. They handle incident response, and remedying threats. Every shift starts with ticket reviews, that are crucial for effective network defense and incident management.

!!! Question "Question 3.1"
    Click on the green View Site button in this task to open the Static Site Lab and navigate to the security monitoring tool on the right panel to try to identify the suspicious activity.

!!! success "Answer"
    No answer needed. Just click the "Answer" button.

Unlike the two first tasks, this actually has some investigation activity to it, however limited. But it all starts somewhere.

![SIEM events](images/siem_alerts.png)

!!! Question "Question 3.2"
    What was the malicious IP address in the alerts?

If we take a look on the image above, we can see the malicious IP address. Well, actually we done know that that the IP is malicious yet, but by clicking the alert, and using the "IP-SCANNER", we can see that it is flagged as malichious.

??? success "Answer"
    221.181.185.159

![Staff members](images/staff_member.png)

Now that we know that the IP address is malicious, we must report it! To whom of the four staff members should the alert be reported to?

!!! Question "Question 3.3"
    To whom did you escalate the event associated with the malicious IP address?

??? success "Answer"
    will griffin

The staff member to whom the event was reported to, has given us permission to block the malicious IP address in the firewall.
We put in the IP address, and get a message.

![Firewall block list](images/ip-blocker.png)

!!! Question "Question 3.4"
    After blocking the malicious IP address on the firewall, what message did the malicious actor leave for you?

??? success "Answer"
    THM{UNTIL-WE-MEET-AGAIN}

## Conclusion

This is a VERY basic insight into the Junior Security Analyst. However, it is clear that it can be really comprehensive and time consuming. Today, more and more of the trivial tasks are automated, but even so, there are many false positives and new corner cases that become more and more complex.