Tags: #tools #threat #intel

# Threat Intelligence Tools
  
Room URL:  [https://tryhackme.com/r/room/threatinteltools](https://tryhackme.com/r/room/threatinteltools)

### Intro

This room let us explore different OSINT tools used to conduct security threat assessments and investigations.

## Task 1 - Room Outline

!!! quote
    This room will cover the concepts of Threat Intelligence and various open-source tools that are useful. The learning objectives include:

    Understanding the basics of threat intelligence & its classifications.
    Using UrlScan.io to scan for malicious URLs.
    Using Abuse.ch to track malware and botnet indicators.
    Investigate phishing emails using PhishTool
    Using Cisco's Talos Intelligence platform for intel gathering.

## Task 2- Threat Intelligence

This section briefly explains what threat intelligence are, and how they are classified into four different categories.

```Strategic intel```
: High-level intel that looks into the organisation's threat landscape and maps out the risk areas based on trends, patterns and emerging threats that may impact business decisions.  

```Technical intel```  
: Looks into evidence and artefacts of attack used by an adversary. Incident Response teams can use this intel to create a baseline attack surface to analyse and develop defence mechanisms.  

```Tactical intel``` 
: Assesses adversaries' tactics, techniques, and procedures (TTPs). This intel can strengthen security controls and address vulnerabilities through real-time investigations.  

```Operational intel```  
: Looks into an adversary's specific motives and intent to perform an attack. Security teams may use this intel to understand the critical assets available in the organisation (people, processes, and technologies) that may be targeted.

## Task 3 - UrlScan.io

This next task, or information if you will, describes what [UrlScan.io](https://urlscan.io/) is, and what it does.

It "simply" scans the website, and provides gatherd information and provide them in a structured format.

This is simply a copy-pasta, as there is no need to re-phrase from TryHackMe.  

**Summary**: Provides general information about the URL, ranging from the identified IP address, domain registration details, page history and a screenshot of the site.  

**HTTP**: Provides information on the HTTP connections made by the scanner to the site, with details about the data fetched and the file types received.  

**Redirects**: Shows information on any identified HTTP and client-side redirects on the site.  

**Links**: Shows all the identified links outgoing from the site's homepage.  

**Behaviour**: Provides details of the variables and cookies found on the site. These may be useful in identifying the frameworks used in developing the site.  

**Indicators**: Lists all IPs, domains and hashes associated with the site. These indicators do not imply malicious activity related to the site.  

### A senario

This is a small eksample of the UrlScanner summary page based on TryHackMe.com.

<figure markdown="span">
    ![UrlScan.io Example](https://tryhackme-images.s3.amazonaws.com/user-uploads/5fc2847e1bbebc03aa89fbf2/room-content/322ccb4ad9e4a6cd7e2998ba6def47ec.png)
</figure>

??? warning "Spoiler"
    <figure markdown="span">
    ![UrlScan.io Example](images/urlscan.png)
    </figure>

!!! question "Question 2.1"
    What was TryHackMe's Cisco Umbrella Rank based on the screenshot?

!!! note "Bonus Info"
    [The Cisco Umbrella ranking list](https://mistyped.one/lists/cisco), is the top 1,000,000 popular websites in the world.

??? success "Answer"
    345612

!!! question "Question 2.2"
    How many domains did UrlScan.io identify on the screenshot?

??? success "Answer"
    13

!!! question "Question 2.3"
    What was the main domain registrar listed on the screenshot?

??? success "Answer"
    NAMECHEAP INC

!!! question "Question 2.4"
    What was the main IP address identified for TryHackMe on the screenshot?

??? success "Answer"
    2606:4700:10::AC43:1b0a

## Task 4 - Abuse.ch

!!! quote
    Initially launched as a private initiative of a random Swiss guy that wanted to fight cyber crime for the good of the internet, abuse.ch is nowadays providing community driven threat intelligence on cyber threats.

    abuse.ch's main goal is to identify and track cyber threats, with a strong focus on malware and botnets. We not only publish actionable threat intelligence data on cyber threats but also develop and operate platforms for IT security researchers and experts enabling them sharing relevant threat intel data with the community.

    Today, data from abuse.ch is already integrated in many commercial and open source security products. Vendors of security software and services rely on our data to protect their customers. But it doesn't stop there: organizations, internet service providers (ISPs), law enforcement and government entities consume data from abuse.ch to fight cyber threats targeting their constituency.

**Malware Bazaar:**  A resource for sharing malware samples.  
**Feodo Tracker:**  A resource used to track botnet command and control (C2) infrastructure linked with Emotet, Dridex and TrickBot.  
**SSL Blacklist:**  A resource for collecting and providing a blocklist for malicious SSL certificates and JA3/JA3s fingerprints.  
**URL Haus:**  A resource for sharing malware distribution sites.  
**Threat Fox:**  A resource for sharing indicators of compromise (IOCs).  
**YARA IFI:** Hunt for suspicious files using YARA. Allows you to share your own YARA rules with the community. (Not actually on the TryHackMe list, as it is rather new.)  

I will not go into further details on the individual sub domains, as this field on its own is huge and this is more of a informative feature.

!!! question "Question 3.1"
    The IOC 212.192.246.30:5555 is identified under which malware alias name on ThreatFox?

To answer this question, we go to the [Threatfox.](https://threatfox.abuse.ch/browse/) website. And put in the IP, and as per search syntax, prefixed with **ioc:**. - ```ioc:212.192.246.30:5555```
<br>

![ThreatFox database](images/ThreatFox-1.png)

And if we click the IOC, we get more information and the answer to the first question.

??? warning "Spoiler"
    <figure markdown="span">
    ![ThreatFox database](images/ThreatFox-2.png)
    </figure>
??? success "Answer"
    Katana

!!! question "Question 3.2"
    Which malware is associated with the JA3 Fingerprint 51c64c77e60f3980eea90869b68c58a8 on SSL Blacklist?

To answer the next question, we go to the [SSL Blacklist](https://sslbl.abuse.ch/ja3-fingerprints/) site.

Once there, we simply put the provided JA3 fingerprint in the sarch field and click the result to get more information.

![alt text](images/ssl-blacklist-1.png)

??? warning "Spoiler"
    <figure markdown="span">
    ![SSL Blacklist](images/ssl-blacklist-2.png)
    </figure>

??? success "Answer"
    Dridex

!!! question "Question 3.3"
    From the statistics page on URLHaus, what malware-hosting network has the ASN number AS14061?

To get the answer on this one, we go to the URLHaus statistics site, and "find/look" for the information. There is no searchbar, but **CRTL-F** does the trick.

??? warning "Spoiler"
    <figure markdown="span">
    ![URLHaus](images/URLHaus.png)
    </figure>

??? success "Answer"
    DIGITALOCEAN-ASN

!!! question "Question 3.4"
    Which country is the botnet IP address 178.134.47.166 associated with according to FeodoTracker?

This question is almost answered in the same manner as before. We navigate to the [FeodoTracker](https://feodotracker.abuse.ch/browse/) and paste the IP into the searchbar. 

??? warning "Spoiler"
    <figure markdown="span">
    ![FeodoTracker](images/feodotracker.png)
    </figure>

??? success "Answer"
    Georgia

## Task 5 - PhishTool

*A VM is attached to this task, start it now, as it takes some time to spin up.*

!!! quote
    This task will introduce you to a tool, PhishTool, that you would add to your toolkit of email analysis tools. Please take note that it would not be necessary to use it to complete the task; however, the principles learnt would be helpful.

Phishing Emails is one, if not THE one biggest precursors of any cyber attack. The technical countermeasures simply cannot catch them all, and the final defence, us, the humans is and will most likely always be the faulty firewall. As a result, adversaries infect their victims’ systems with malware, harvesting their credentials and personal data and performing other actions such as financial fraud or conducting ransomware attacks.

There are five other rooms with more in-depth informataion.

[Phishing Emails 1](https://tryhackme.com/room/phishingemails1tryoe)  
[Phishing Emails 2](https://tryhackme.com/room/phishingemails2rytmuv)  
[Phishing Emails 3](https://tryhackme.com/room/phishingemails3tryoe)  
[Phishing Emails 4](https://tryhackme.com/room/phishingemails4gkxh)  
[Phishing Emails 5](https://tryhackme.com/room/phishingemails5fgjlzxc)  

PhishTool comes in two variants. The community edition and the Enterprise edition. We will focus on the community edition.

To complete the tasks, a useraccount on the [PhishTool](https://app.phishtool.com/sign-up/community) website is necessary.
The core features and how it works will not be covered, to any other extend, then what the tasks require.

### Scenario
You are a SOC Analyst and have been tasked to analyse a suspicious email, Email1.eml. To solve the task, open the email using Thunderbird on the attached VM, analyse it and answer the questions below. (As stated in the beginning, a VM is attached to this task.)

!!! question "Question 5.1"
    What social media platform is the attacker trying to pose as in the email?

To get started, open the **Emails** folder and then open the Email1.eml file. The tasks says to open in thunderbird, but opening in a text editor and copy pasting to a txt file in another VM is ALOT faster. (**Faster**, not necessarily safer.)

<figure markdown="span">
    ![Email folder](images/phishtool-1.png)
</figure>
<figure markdown="span">
    ![Email folder](images/phishtool-2.png)
</figure>

Once the content of the email is pasted into a text file, it can be uploaded for analysis in the PhishTool. Either by dragging and dropping, or clicking the **Choose file** button.  

<figure markdown="span">
    ![PhishTool](images/phishtool-3.png)
</figure>

Now, on the very first page we get a rendered version of the email. The *hint* indicates a very familiar logo, but in my case at least, there was no obvious *logo*. HOWEVER! In the bottom of the eamil we get a hint of, where the email *originates* from.  

??? warning "Spoiler!"
    <figure markdown="span">
        ![PhishTool rendered](images/phishtool-4.png)
    </figure>

??? success "Answer"
    LinkedIn

**Onwards!**

!!! question "Question 5.2"
    What is the senders email address?

Now, if we look at the right side we can see the headers of the original email, and the very first line tells us the senders email address.
Read about headers [here](https://support.clickdimensions.com/hc/en-us/articles/360031909032-Understanding-Email-Headers).

??? warning "Spoiler!"
    <figure markdown="span">
        ![PhishTool Headers](images/phishtool-5.png)
    </figure>

??? success "Answer"
    darkabutla@sc500.whpservers.com

!!! question "Question 5.3"
    What is the senders email address?

The next question is answered two lines below, on the same page.

??? warning "Spoiler!"
    <figure markdown="span">
        ![PhishTool Headers](images/phishtool-6.png)
    </figure>

??? success "Answer"
    cabbagecare@hotsmail.com

**EASY!**

Now, if we look at the next question we can see that we need to find a IP address.

!!! question "Question 5.4"
    What is the Originating IP address? Defang the IP address.

To defang the origination IP address, we have to take a look at the x-header to find the IP address. Scroll to the *x-sender-ip* and defang it. CyberChef will do it, but its really easy.

!!! note
    Defanging is done to prevent accidental click of malicious links and addresses. In our case, an IP address.

??? warning "Spoiler!"
    <figure markdown="span">
        ![alt text](images/phishtool-7.png)
    </figure>

??? success "Answer"
    204[.]93[.]183[.]11

Now, last question...

!!! question "Question 5.5"
    How many hops did the email go through to get to the recipient?

To answer the last question in this sub-section we take a look at the *Recieved lines*. Here we simply count how many hops the email went through.

??? warning "Spoiler!"
    <figure markdown="span">
        ![alt text](images/phishtool-8.png)
    </figure>

??? success "Answer"
    4

**Now, we can add the PhishTool to out toolbox!**

## Task 6 - Cisco Talos Intelligence



## Scenario 1

## Scenario 2

## Conclusion