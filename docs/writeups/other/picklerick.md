Tags: #nikto #nmap

# Pickle Rick

Room URL: [https://tryhackme.com/room/picklerick](https://tryhackme.com/room/picklerick)

## Intro

This Rick and Morty-themed challenge requires you to exploit a web server and find three ingredients to help Rick make his potion and transform himself back into a human from a pickle.

Deploy the virtual machine on this task and explore the web application: 10.10.237.51

## Enumeration

Basic enumeration

### NMAP

Even though i dont really know if nmap would find anything, it is a good thing to get going early on, as it can take some time.

```bash
nmap -sC -sV $10.10.237.51
```

**Findings:**

SSH on port 22 and web on port 80

```bash
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.6 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 6c:05:93:32:b3:76:85:1d:e4:ae:3a:37:11:98:a1:39 (RSA)
|   256 03:22:9c:25:66:d3:7b:6c:52:2d:d8:26:f9:63:c8:2c (ECDSA)
|_  256 4d:8d:36:bb:3c:5c:d6:bf:86:9e:7e:d1:09:99:35:e6 (ED25519)
80/tcp open  http    Apache httpd 2.4.18 ((Ubuntu))
|_http-server-header: Apache/2.4.18 (Ubuntu)
|_http-title: Rick is sup4r cool
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

### Web application

The intro already hints us, that there is a web application.

The site it-self does not seap much other then we are looking for a password.

#### Web application source code

There is not much to look at in the source code of the web site, however, there is a "note to self" comment, leaking a username:

```html
 <!--

    Note to self, remember username!

    Username: R1ckRul3s

 -->
```

That is about all we get.

### robot.txt

Is there a robots.txt file? This can sometimes aide in enumerating directories.

Ohh well! There IS a robots.txt file, however, it only contains "Wubbalubbadubdub". That surely cant be of any use....

### Nikto

Lets start nikto, and scan web-servers for vulnerabilities that can help us compromise the server.
Nikto also does some basic directory structuring.

Findings:

At first glance the most interesting part is the "/login.php"

```bash
└─$ nikto -h http://10.10.237.51
- Nikto v2.5.0
---------------------------------------------------------------------------
+ Target IP:          10.10.237.51
+ Target Hostname:    10.10.237.51
+ Target Port:        80
+ Start Time:         2023-09-15 15:46:44 (GMT-4)
---------------------------------------------------------------------------
+ Server: Apache/2.4.18 (Ubuntu)
+ /: The anti-clickjacking X-Frame-Options header is not present. See: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Frame-Options
+ /: The X-Content-Type-Options header is not set. This could allow the user agent to render the content of the site in a different fashion to the MIME type. See: https://www.netsparker.com/web-vulnerability-scanner/vulnerabilities/missing-content-type-header/
+ No CGI Directories found (use '-C all' to force check all possible dirs)
+ /: Server may leak inodes via ETags, header found with file /, inode: 426, size: 5818ccf125686, mtime: gzip. See: http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2003-1418
+ Apache/2.4.18 appears to be outdated (current is at least Apache/2.4.54). Apache 2.2.34 is the EOL for the 2.x branch.
+ OPTIONS: Allowed HTTP Methods: GET, HEAD, POST, OPTIONS .
+ /login.php: Cookie PHPSESSID created without the httponly flag. See: https://developer.mozilla.org/en-US/docs/Web/HTTP/Cookies
+ /icons/README: Apache default file found. See: https://www.vntweb.co.uk/apache-restricting-access-to-iconsreadme/
+ /login.php: Admin login page/section found.
+ 8075 requests: 0 error(s) and 8 item(s) reported on remote host
+ End Time:           2023-09-15 15:59:43 (GMT-4) (779 seconds)
---------------------------------------------------------------------------
+ 1 host(s) tested
```

#### /login.php

The site presents us with a site with "Username" and "Password" input fields.

Lets see what we can make of it.

We got a username "R1ckRul3s", and a string of text from the robots.txt file "Wubbalubbadubdub".

That did the trick!

Now we are presented with a "Command Panel". A quick test with "whoami" shows that we can run shell commands on the server. Really bad design... however! Using "ls" reveals both a "clue.txt" and a file called "Sup3rS3cretPickl3Ingred.txt"

Apparently we a not allowed to use "cat" to read the text files. Surely we cant use vim or nano, but less is more i guess.

By using "less" on Sup3rS3cretPickl3Ingred.txt, we get the output "mr. meeseek hair"

This must be the first ingredient!

Now, using less on clue.txt gives us the clue to look aronud the file system.

I used "ls" to see if we actually could look around the system, and it turns out we can!

Turns out "rick" is a user on the system, and there is a file called "second ingredient". Lets "less" that and move on.

"less ../../../../home/rick/\"second ingredients\"\"

Remember the double quotes, as the filename has a space....

**1 jerry tear**

For the third ingredient, i wanted to look around in the root folder, but everything came up empty. Then i remembered to use "sudo", as i was looking in root folders...

Now i could se 3rd.txt!

sudo less it, and be done!

**3rd ingredients: fleeb juice**

## Reflections

[John Hammonds](https://www.youtube.com/watch?v=oCAtfcr3iUw) walkthrough covers a few other ways this could be done and. His instant shell is fantastic! Deffinently look more into that! 

In [Jaspers](https://medium.com/@JAlblas/tryhackme-pickle-rick-walkthrough-5f79716a86dc) writeup, he finds a list of blocked words by using "tac" instead of "less" on portal.php. This would have made things easier, especially as he reveals with sudo -l, that www-data can actually run all commands. John also mentions this, but at this point he has already gained a shell.

### Other

Dirbuster or similar could probably have been used to enumerate directories, but Nikto did a good enough job.