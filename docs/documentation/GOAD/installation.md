# GOAD Installation

Jeg fulgte indledningsvis [denne](https://mayfly277.github.io/posts/GOAD-on-proxmox-part1-install/) guide. Efter at have konstateret at pfSense langt om længe ville makke ret, var der dog stadig mange issues der skulle debugges. Jeg gik oprindeligt med OPNsense da det er brugt i undervisningen, men skiftede til pfSense for at udelukke DET skulle være problemet. Efter alt for mange spildte timer er der dog noget der tyder på, at det er intern routing og interne firewall regler i proxmox der spænder ben. (Det kan ikke udelukkes at den utroligt dårlige hjemme-wifu-router også kan have en bit med i spillet)

**Installation af GOAD er henlagt for nu**