# The Diamond Model

## Hvad er "The Diamond Model"?

Der er ikke tale om Michael Porter's "Diamond Model"...

[Denne Diamond Model](https://threatintelligenceplatform.com/blog/cyber-threat-intelligence-models-the-diamond-model) fokuserer på at identificere og analysere fire hovedkomponenter i et cyberangreb, men dækker meget mere end bare dét. Modellen hjælper med, at visualisere og forstå dynamikken i et angreb ved at identificere sammenhængen mellem disse fire hovedkomponenter. Man kan altså roligt sige at der er tale om en analysemodel.

**Adversary:** Dette refererer til den angriber eller trusselsaktør, der udfører angrebet. Det omfatter identifikation af angriberens motiver, færdigheder, ressourcer og taktikker.

**Infrastructure:** Dette omfatter de ressourcer og midler, som angriberen bruger til at udføre angrebet. Det kan omfatte IP-adresser, domænenavne, botnet-kontrolservere, værktøjer og teknikker brugt af angriberen.

**Capabilities:** Dette refererer til de specifikke tekniske evner og metoder, som angriberen udnytter under angrebet. Dette kan omfatte udnyttelsesværktøjer, malware-funktionalitet, kompromitteringsmetoder osv.

**Victim:** Dette er den målrettede organisation eller enhed, der er målet for angrebet. Det omfatter identifikation af den angrebene enheds netværksinfrastruktur, systemer, data og aktiver.

Modellen er nyttig for at forstå, hvordan et cyberangreb udspiller sig, identificere mønstre i angrebene og opdage forbindelser mellem forskellige trusselsaktører, infrastrukturer og angrebsscenarier.

## Hvad kan den bruges til?

Den er med til at skabe en holistisk forståelse af de tekniske aspekter i et angreb, en struktureret analyse med en klar og organiseret måde at behandle de indsamlede dataer der både kan benyttes før et angreb (Threat Intelligence) og efter et angreb (Incident Response).
