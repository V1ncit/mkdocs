# Pyramid of Pain

Jeg blev først opmærksom på modellen i TryHackMe's SOC Level 1 Learning Path.  
Rummet findes her: [https://tryhackme.com/r/room/pyramidofpainax](https://tryhackme.com/r/room/pyramidofpainax)  
Writeup af rummet kan findes her: [https://v1ncit.gitlab.io/mkdocs/writeups/pyramidofpain/](https://v1ncit.gitlab.io/mkdocs/writeups/pyramidofpain/).
<figure markdown="span">
![](https://www.attackiq.com/wp-content/uploads/2019/06/blog-pyramid-pain-01-768x432.jpg)
</figure>
Pyramid of Pain er en model opfundet af [David J. Bianco](https://www.linkedin.com/in/davidjbianco/), der illustrerer forskellige typer af Indicators of Compromise (IoC) og forholdet mellem typen af IoC, hvor svært det er for et SOC at identificere og hvor svært det er for en trusselsaktør at omgå. Har SOC'en styr på hele pyramiden for en trusselsaktør, gør de det VIRKELIG vanskeligt at opnå deres mål.

**Egne tanker**

Modellen giver utroligt meget mening, og den er også med til at vise hvor ressourcerne og energien skal bruges hvis man vil gøre det RIGTG svært for en trusselsaktør. Den viser også, hvor svært det er at opnå, at kunne finde IoC'erne. Det er en model jeg ikke selv har hørt om før, og jeg har ingen ide om, hvor udbredt den er.