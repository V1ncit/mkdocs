# Selvvalgt fordybelse

Denne sektion vil dække over dokumentation og opdagelser ifbm. semester projektet "Selvvalgt fordybelse" hvor **Cyber Threat Intelligence** er hovedemnet.

# Hvad er intelligence?

## Cyber Threat Intelligence
Cyber Threat Intelligence er data der er samlet, processeret og analyseret for at prøve at forstå en aktørs motiver, mål og handlemønstre (TTP'er). Informationer er altså ikke efterretninger før de er analyseret. Threat Intelligence gør os i stand til at handle og træffe beslutninger hurtigere, på et oplyst grundlag med baggrund i valide dataer. Det gør os også i stand til at bevæge os fra en reaktiv til en proaktiv strategi. Termen "Cyber", er ganske enkelt fordi vi i denne sammenhæng beskæftiger os med aktører der operere i cyberspace. 

### Intelligence Life Cycle

Efterretningscyklussen, er en metodisk tilgang til at håndtere informationer og producere brugbar efterretning. Disse faser hjælper med til at organisere indsatsen for at indsamle, analysere og udnytte de informationer man har fået indsamlet.

![Intelligence Life Cycle](threat-intelligence-lifecycle-3.jpg)
*[www.crowdstrike.com](https://www.crowdstrike.com/cybersecurity-101/threat-intelligence/)*

#### 1. Planlægning og identificering af behov

Før indsamling af data bør begynde, er det vigtigt at identificere og prioritere specifikke informationsbehov. Dette indebærer blandt andet at definere, hvad der ønskes at vide eller opnå gennem informationerne, såsom at forstå trusselsbilledet, herunder aktører, motiver, mål og midler. Samt at vurdere risici og identificere potentielle trusler for at støtte i træfningen af beslutninger.

I denne verden er der dog undtagelser. Det sker ofte at vi får en masse information i forbindelse med et angreb, enten mens det står på, eller efterfølgende som en del af klarlægning af hændelsesforløbet.

#### 2. Indsamling

Anden fase er indsamling af informationer fra forskellige kilder. Dette kan omfatte kilder som trusselsrapporter, hændelsesrapporter (*[osinter](https://osinter.dk)*), sikkerhedsblogs, monitorering på dark-web (*[flare](https://flare.io/solutions/use-case/dark-web-monitoring/)*) og lignende. Det kan også omfatte lukkede kilder som deling af informationer med andre sikkerhedsorganisationer eller efterretningsrapporter. 

Det er vigtigt at forstå, at de indsamlede informationer altså ikke "efterretninger" endnu.

#### 3. Bearbejdning

Efter at informationerne er indsamlet, gennemgår man bearbejdningsfasen. Her organiseres, strukturers og formateres informationerne så de er klar til analyse. Dette kan indebære verificering af kildernes troværdighed og eventuelt fjerne unødvendige oplysninger (støj). Det kan også indebære at skulle oversætte dokumenter på fremmedsprog og dekryptere filer.

#### 4. Analyse

Analysefasen er den fase der omdanner de behandlede informationer til efterretninger. Nøgle aspekterne i denne analyse er mønsteranalyse, trusselsmodellering, profilering af trusselsaktører for blandt andet at klarlægge metoder, mål, midler og handlemønstre (TTP).

Her kan man benytte frameworks som "Dimond Model", "Cyber Kill Chain" og [MITRE ATT&CK](https://attack.mitre.org/) for at strukturer og visualisere de dataer man har udledt af de informationer der er blevet behandlet.

#### 5. Formidling

I denne fase er det analytikerne der skal omsætte deres analyser, til et, afhængigt af modtagerne, passende format, således det er let at forstå. Hvordan formatet og præsentationen er formuleret, afhænger af hvem modtageren er. Det ved man heldigvis fra planlægningsfasen. Det er også i denne fase man konkludere i rapporten eller præsentationen, om man har fået dækket det behov man identificerede, eller nåede det mål man satte i planlægningsfasen.

Formidlingen skal gøres på en sikker måde, således de nu kategoriseret og **KLASSIFICEREDE** efterretninger ikke bliver en ny trussel. Den mere og mere udbredte måde at klassificere rapporter på, er ved hjælp af Traffic Light Protocol (*[CFCS](https://www.cfcs.dk/da/handelser/traffic-light-protocol/)*).

#### 6. Feedback

Den sidste fase indebære at få feedback på det produkt man har produceret og formidlet. Har det dækket de behov og mål som man definerede i planlægningsfasen. Er det formidlet korrekt og til tiden. Og er der forbedringer der kan tages med ved næste cyklus.