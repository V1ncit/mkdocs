# Caldera

*"Caldera™ is an adversary emulation platform designed to easily run autonomous breach-and-attack simulation exercises. It can also be used to run manual red-team engagements or automated incident response. Caldera is built on the MITRE ATT&CK™ framework and is an active research project at MITRE."* - [MITRE Caldera](https://caldera.readthedocs.io/en/latest/)
